BLUE='\033[0;34m'
NC='\033[0m' # No Color

ifeq ($(BITNAMI_SEALED_SECRETS_VER),)
BITNAMI_SEALED_SECRETS_VER := "0.15.0"
endif

help: ## Show help
	@grep --no-filename -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: all
.SILENT: all
all: install-ansible-kubernetes install-cluster deploy-kubernetes-manifests ## Install requirements, deploy k8s cluster and gitops tool with application manifest


.PHONY: install-cluster
.SILENT: install-cluster
install-cluster: ## Install K8s cluster
	@echo "\n${BLUE}Installing MicroK8s cluster...${NC}\n"
	ansible-playbook -i inventory.yaml roles/main.yml --extra-vars wifi_password=""  --extra-vars wifi_ssid=xxx1
	kubectl label node node1 role=capitan
	kubectl label node node2 role=multimedia
	kubectl label node node3 role=airmonitor
	kubectl label node node4 role=airmonitor

.PHONY: deploy-kubernetes-manifests
.SILENT: deploy-kubernetes-manifests
deploy-kubernetes-manifests: ## Install GitOps tool - ArgoCD and deploy application definitions
	@echo "\n${BLUE}Deploying GitOps tool - ArgoCD with application definitions...${NC}\n"
	kubectl create ns gitlab
	ansible-galaxy collection install community.kubernetes
	ansible-playbook  roles/kubernetes.yml --verbose
