#!/bin/bash
sudo snap stop microk8s
sleep 120
sudo snap remove microk8s --purge
sleep 120
sudo snap install microk8s --classic --channel=1.21
microk8s status --wait-ready
microk8s disable ha-cluster --force
microk8s status --wait-ready