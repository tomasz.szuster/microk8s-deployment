    createuser -drs openproject -h 192.168.1.171 --username=nextcloud
    createdb -O openproject openproject -h 192.168.1.171 --username=nextcloud
    psql -h 192.168.1.171 -U nextcloud -d openproject -W
    ALTER USER openproject WITH ENCRYPTED password 'changeme';
    \q