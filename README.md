# Ubuntu's MicroK8s kubernetes cluster via ansible

## Pre-requisites
* edit **inventory.yaml** to update IP addresses for your nodes

## Tested on
* ansible-playbook ver 2.9.27
* python ver 3.8.2
* microk8s snap ver 1.21/stable
* Raspberry Pi 4 8GB RAM
* [PoE hat](https://allegro.pl/oferta/modul-raspberry-poe-hat-fundacji-wer-zmodyfikowana-7745279107?utm_source=notification&utm_medium=paymentPaidBuyer&utm_campaign=payment-paid-for-buyer)
* [SSD to USB adapter](https://allegro.pl/oferta/adapter-ssd-m-2-usb-3-0-ngff-obudowa-m2-sata-9554014053?utm_source=notification&utm_medium=buyerCart&utm_campaign=transaction-notifications-buyercart&snapshot=MjAyMS0wNS0wN1QxNzoyNDozMy45OTBaO2J1eWVyO2EwMmNjODI4ZTkzMzU4YzJlMDA1MDhiNDQ5NjM3MGE1YmQxNWJkMDgxMTlhNjM0M2ExZDRmZjQ4Yjk2OWUzZWE%3D)
* [SSD drive 128GB](https://allegro.pl/oferta/nowy-dysk-ssd-128-gb-m2-sata-iii-2280-10454244960?utm_source=notification&utm_medium=buyerCart&utm_campaign=transaction-notifications-buyercart&snapshot=MjAyMS0wNS0xNFQwNDozMzowMC41MTBaO2J1eWVyOzY2NGEzNGU0OTIyMWU4OWNiZWM3ZGY2MjJmNWU0ZDBiOWQ0MjYyZjQ2NTEwYmE4YTRmMjAzNDMwNWFmYmYyZTM%3D)

## Default settings
K8s cluster ver 1.21

Listed K8s services will be enabled during installation
* dns
* ingress
* metallb
* metrics-server
* prometheus
* storage

Please edit **roles/microk8s-services/tasks/main.yml** if you want to add/remove any functionality

The configuration file for K8s cluster will be downloaded to ~/.kube/microk8s

## Installation
**By default ansible will use 6 nodes.**

### All in one go installation

    make all # TL;DR - If you want to install all in one go

### Step-by-step installation

**Install dependencies**

    make install-ansible-kubernetes

**Install K8s cluster**

    make install-cluster

**Install GitOps Tool - ArgoCD and deploy application definitions**

    make deploy-kubernetes-manifests

If you want to use bigger/smaller cluster then please adjust inventory file and **microk8s-nodes/tasks/main.yml** to add/remove 
```yaml
- name: Generate K8s join token
  include: generate_token.yml

- name: Join nodes to cluster
  command: "{{ hostvars[groups['kube-master'][0]]['join_command'] }}"
  when: inventory_hostname == groups['kube-node'][4]

```

[4] means that this is 5th worker node in a cluster. 

Thus you need to remove or add as many above yaml statements as needed to increment value in []


Please be patient, installation can take few hours to complete.

During installation there will be couple node reboots, thus ansible will be reporting connection errors. 

**Search for metallb**

By default one address pool with range 192.168.1.170-192.168.1.195 will be created.

## Addons
### Grafana
The Grafana visualization tool will be installed via helm chart with couple dashboards listed in **helm-values/grafana.yaml**.

The grafana.192.168.1.186.nip.io domain will be used by the ingress to assign it to Grafana service.

### ArgoCD
The ArgoCD with version 1.8.7 will be installed using argocd.192.168.1.186.nip.io domain.


### ArgoCD Notifications
The ArgoCD Notifications version 0.7.0 will be installed in K8s argocd namespace.

Argo CD Notifications continuously monitors Argo CD applications and provides a flexible way to notify users about important changes in the application state.

Please apply your own secret

```bash
kubectl apply -n argocd -f - << EOF
apiVersion: v1
kind: Secret
metadata:
  name: argocd-notifications-secret
stringData:
  notifiers.yaml: |
    slack:
      token: <my-token>
type: Opaque
EOF
```

and update ArgoCD application to push notifications:

```bash

kubectl patch app <my-app> -n argocd -p '{"metadata": {"annotations": {"recipients.argocd-notifications.argoproj.io":"slack:<my-channel>"}}}' --type merge

```

Updates to Kube-proxy to get metallb working
    cd /var/snap/microk8s/current/args/
    cat kube-proxy
    --kubeconfig=${SNAP_DATA}/credentials/proxy.config
    --cluster-cidr=10.1.0.0/16
    --healthz-bind-address=127.0.0.1
    --ipvs-strict-arp
    root@node1:/var/snap/microk8s/2544/args# pwd
    /var/snap/microk8s/2544/args